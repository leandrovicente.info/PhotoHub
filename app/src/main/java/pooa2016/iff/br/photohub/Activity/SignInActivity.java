package pooa2016.iff.br.photohub.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import pooa2016.iff.br.photohub.R;

public class SignInActivity extends AuthActivity {

    public final static String EXTRA_MESSAGE = "pooa2016.iff.br.photohub.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
    }

    public void auth(View view){

        EditText txtEmail = (EditText) findViewById(R.id.txtEmail);
        EditText txtPassword = (EditText) findViewById(R.id.txtPassword);

        String email = txtEmail.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();

        /*Validation */
        if (email.isEmpty() || email == null){
            Toast.makeText(this,"Please, provide an email", Toast.LENGTH_LONG).show();
            return;
        }

        if (password.isEmpty() || password == null){
            Toast.makeText(this,"Please, provide a password", Toast.LENGTH_LONG).show();
            return;
        }

        super.signIn(email,password);
        finish();
       }

    public void cancel(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
