package pooa2016.iff.br.photohub.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.util.List;

import pooa2016.iff.br.photohub.Extra.Session;
import pooa2016.iff.br.photohub.Model.Client;
import pooa2016.iff.br.photohub.Model.Photographer;

public class AuthActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void signIn(String email, String password){
        List<Photographer> photographer = null;
        List<Client> client = null;

        // Looks up a photographer instance first
        try{
            photographer = Photographer.find(Photographer.class,"email = ? and password = ?",email,password);
        }catch(Exception e){
            Toast.makeText(this, "Failed to access photographers table in database", Toast.LENGTH_LONG).show();
        }

        if (!photographer.isEmpty()){
            Session.setUser(this,Integer.parseInt(photographer.get(0).getId().toString()),"photographer");

            Intent intent = new Intent(this,HomePhotographerActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        // If it is not a photographer then it checks against a client
        try{
            client = Client.find(Client.class,"email = ? and password = ?",email,password);
        }catch (Exception e){
            Toast.makeText(this, "Failed to access clients table in database", Toast.LENGTH_LONG).show();
        }

        if (!client.isEmpty()){
            Session.setUser(this,Integer.parseInt(client.get(0).getId().toString()),"client");

            Intent intent = new Intent(this,HomeClientActivity.class);
            startActivity(intent);
            finish();
            return;
        }
        // If nothing is found then shows a message
        Toast.makeText(this, "Wrong email/password combination or user not registered", Toast.LENGTH_LONG).show();
    }
}
