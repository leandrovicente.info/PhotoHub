package pooa2016.iff.br.photohub.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import pooa2016.iff.br.photohub.Model.Client;
import pooa2016.iff.br.photohub.R;

public class RegisterClientActivity extends AuthActivity {

    Button btnSubmit;
    EditText name, phone, country, state, city, email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_client);

        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener(){
            @Override

            public void onClick(View view){
                save();
            }
        });
    }

    public void save(){
        name = (EditText) findViewById(R.id.eName);
        phone = (EditText) findViewById(R.id.ePhone);
        country = (EditText) findViewById(R.id.eCountry);
        state = (EditText) findViewById(R.id.eState);
        city = (EditText) findViewById(R.id.eCity);
        email = (EditText) findViewById(R.id.eEmail);
        password = (EditText) findViewById(R.id.ePassword);

        Client client = new Client(
                name.getText().toString(),
                phone.getText().toString(),
                city.getText().toString(),
                state.getText().toString(),
                country.getText().toString(),
                email.getText().toString(),
                password.getText().toString()
        );

        try{
            client.save();
            Toast.makeText(this,"Welcome to PhotoHub!", Toast.LENGTH_LONG).show();
            super.signIn(client.getEmail(),client.getPassword());
        }catch (Exception e){
            Toast.makeText(this,"Failed to create your account!", Toast.LENGTH_LONG).show();
        }

    }


}
