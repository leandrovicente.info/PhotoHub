package pooa2016.iff.br.photohub.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

import pooa2016.iff.br.photohub.Extra.Session;
import pooa2016.iff.br.photohub.Model.Client;
import pooa2016.iff.br.photohub.Model.Photographer;
import pooa2016.iff.br.photohub.Model.PhotographerReview;
import pooa2016.iff.br.photohub.R;

public class NewPhotographerReviewActivity extends AppCompatActivity {

    TextView txtDisplayName, txtBio, txtLocation, txtContact, txtPortfolio;
    RatingBar rRating;
    EditText eComment;
    Button btnSubmitReview, btnCancelReview;

    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_photographer_review);

        final Client client = Client.findById(Client.class,Session.getUser(this));

        Intent intent = getIntent();
        id = (int) intent.getSerializableExtra("id");

        final Photographer photographer = Photographer.findById(Photographer.class,id);

        String displayName = photographer.getDisplayName();
        String bio = photographer.getBio();
        String city = photographer.getCity();
        String state = photographer.getState();
        String country = photographer.getCountry();
        String phone = photographer.getPhone();
        String email = photographer.getEmail();
        String portfolio = photographer.getPortfolio();

        String location = city + " - " + state + ", " + country;
        String contact = phone + " | " + email;

        txtDisplayName = (TextView) findViewById(R.id.txtName);
        txtDisplayName.setText(displayName);

        txtBio = (TextView) findViewById(R.id.txtBio);
        txtBio.setText(bio);

        txtLocation = (TextView) findViewById(R.id.txtLocation);
        txtLocation.setText(location);

        txtContact = (TextView) findViewById(R.id.txtContact);
        txtContact.setText(contact);

        txtPortfolio = (TextView) findViewById(R.id.txtPortfolio);
        txtPortfolio.setText(portfolio);

        btnSubmitReview = (Button) findViewById(R.id.btnSubmitReview);

        btnSubmitReview.setOnClickListener(new View.OnClickListener(){

            public void onClick(View view){
                rRating = (RatingBar) findViewById(R.id.rRating);
                eComment = (EditText) findViewById(R.id.eComment);

                String comment = eComment.getText().toString();
                String date;

                int rating = Math.round(rRating.getRating());

                SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
                date = s.format(new Date());

                try{
                    PhotographerReview review = new PhotographerReview(comment, date, rating, client,photographer);
                    review.save();
                    Toast.makeText(NewPhotographerReviewActivity.this, "Review added!", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(NewPhotographerReviewActivity.this,PhotographerActivity.class);
                    intent.putExtra("id",String.valueOf(id));
                    startActivity(intent);
                    finish();
                }catch (Exception e){
                    Toast.makeText(NewPhotographerReviewActivity.this, "Failed to add review!", Toast.LENGTH_LONG).show();

                }
            }
        });

        btnCancelReview = (Button) findViewById(R.id.btnCancelReview);

        btnCancelReview.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent intent = new Intent(NewPhotographerReviewActivity.this,PhotographerActivity.class);
                intent.putExtra("id",String.valueOf(id));
                startActivity(intent);
                finish();
            }
        });

    }
}
