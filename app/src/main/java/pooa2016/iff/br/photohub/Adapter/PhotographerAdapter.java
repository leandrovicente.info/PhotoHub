package pooa2016.iff.br.photohub.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import pooa2016.iff.br.photohub.Model.Photographer;
import pooa2016.iff.br.photohub.R;

/**
 * Created by levi on 26/09/16.
 */
public class PhotographerAdapter extends ArrayAdapter<Photographer> {

    private Context context = null;
    private ArrayList<Photographer> photographers = null;

    public PhotographerAdapter(Context context, ArrayList<Photographer> photographers){
        super(context, R.layout.photographers_list, photographers);
        this.context = context;
        this.photographers = photographers;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.photographers_list, parent, false);

        TextView displayName = (TextView) rowView.findViewById(R.id.txtQuoteName);
        TextView city = (TextView) rowView.findViewById(R.id.txtCity);
        TextView bio = (TextView) rowView.findViewById(R.id.txtAuthor);
        TextView id = (TextView) rowView.findViewById(R.id.txtId);

        displayName.setText(photographers.get(position).getDisplayName());
        city.setText(photographers.get(position).getCity());
        bio.setText(photographers.get(position).getBio());
        id.setText(photographers.get(position).getId().toString());

        return rowView;
    }
}
