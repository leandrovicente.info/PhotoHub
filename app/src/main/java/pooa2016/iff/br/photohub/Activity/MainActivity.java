package pooa2016.iff.br.photohub.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import pooa2016.iff.br.photohub.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void showLogin(View view){
        Intent intent = new Intent(this,SignInActivity.class);
        startActivity(intent);
    }

    public void showRegisterPhotographer(View view){
        Intent intent = new Intent(MainActivity.this, RegisterPhotographerActivity.class);
        startActivity(intent);
    }

    public void showRegisterClient(View view){
        Intent intent = new Intent(MainActivity.this, RegisterClientActivity.class);
        startActivity(intent);
    }

}
