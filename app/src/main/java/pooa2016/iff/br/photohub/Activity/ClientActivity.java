package pooa2016.iff.br.photohub.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pooa2016.iff.br.photohub.Adapter.ClientReviewsAdapter;
import pooa2016.iff.br.photohub.Model.Client;
import pooa2016.iff.br.photohub.Model.ClientReview;
import pooa2016.iff.br.photohub.R;

public class ClientActivity extends AppCompatActivity {

    TextView txtName, txtContact, txtLocation;
    Button btnAddReview, btnReturn;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);

        //Get ID of selected client and retrieves its records from database
        Intent intent = getIntent();
        String raw_id = (String) intent.getSerializableExtra("id");
        id = Integer.parseInt(raw_id);
        Client client = Client.findById(Client.class,id);

        //Fills in list of client reviews
        List<ClientReview> reviews = client.getClientReviews();
        ListView list = (ListView) findViewById(R.id.lvClientReviews);
        ArrayAdapter<ClientReview> adapter = new ClientReviewsAdapter(this, (ArrayList<ClientReview>) reviews);
        try{
            list.setAdapter(adapter);
        }catch (Exception e){
            Toast.makeText(this, "Failed to load reviews"+e.getMessage(), Toast.LENGTH_LONG).show();
        }

        //Displays client info on page
        String name = client.getName();
        String city = client.getCity();
        String state = client.getState();
        String country = client.getCountry();
        String phone = client.getPhone();
        String email = client.getEmail();

        String location = city + " - " + state + ", " + country;
        String contact = phone + " | " + email;

        txtName = (TextView) findViewById(R.id.txtName);
        txtName.setText(name);
        
        txtLocation = (TextView) findViewById(R.id.txtLocation);
        txtLocation.setText(location);

        txtContact = (TextView) findViewById(R.id.txtContact);
        txtContact.setText(contact);

        //Define action to add new review
        btnAddReview = (Button) findViewById(R.id.btnAddReview);
        btnAddReview.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){

                Intent intent = new Intent(ClientActivity.this, NewClientReviewActivity.class);
                intent.putExtra("id",id);
                startActivity(intent);
                finish();
            }
        });


    }
}
