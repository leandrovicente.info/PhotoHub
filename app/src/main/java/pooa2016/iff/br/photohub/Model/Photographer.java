package pooa2016.iff.br.photohub.Model;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by levi on 12/09/16.
 */
public class Photographer extends SugarRecord {

    private String name;
    private String phone;
    private String country;
    private String state;
    private String city;
    private String bio;
    private String displayName;
    private String portfolio;
    private String email;
    private String password;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(String portfolio) {
        this.portfolio = portfolio;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<PhotographerReview> getPhotographerReviews(){
        return PhotographerReview.find(PhotographerReview.class, "photographer = ?", String.valueOf(getId()));
    }

    public List<ClientReview> getClientReviews(){
        return ClientReview.find(ClientReview.class, "photographer = ?", String.valueOf(getId()));
    }

    public List<PriceQuote> getPriceQuotes(){
        return PriceQuote.find(PriceQuote.class,"photographer = ?", String.valueOf(getId()));
    }

    public Photographer(){

    }

    public Photographer(String name, String phone, String city, String state, String country, String email, String password, String portfolio, String bio, String displayName){
        this.name = name;
        this.phone = phone;
        this.country = country;
        this.state = state;
        this.city = city;
        this.bio = bio;
        this.displayName = displayName;
        this.portfolio = portfolio;
        this.email = email;
        this.password = password;

    }
}
