package pooa2016.iff.br.photohub.Extra;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by levi on 30/09/16.
 */
public class Session {

    private static String PREF_NAME = "prefs";

    private static SharedPreferences getPrefs(Context context){
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static int getUser(Context context){
        return getPrefs(context).getInt("userid",1);
    }

    public static String getType(Context context) {return getPrefs(context).getString("type","error");}

    public static void setUser(Context context, int id, String type){
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putInt("userid",id);
        editor.putString("type",type);
        editor.commit();
    }



}
