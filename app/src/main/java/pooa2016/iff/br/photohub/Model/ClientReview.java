package pooa2016.iff.br.photohub.Model;

import com.orm.SugarRecord;

/**
 * Created by levi on 30/09/16.
 */
public class ClientReview extends SugarRecord{

    private String comment,date;

    private int rating;

    private Client client;

    private Photographer photographer;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Photographer getPhotographer() {
        return photographer;
    }

    public void setPhotographer(Photographer photographer) {
        this.photographer = photographer;
    }

    public ClientReview(){}

    public ClientReview(String comment, String date, int rating, Photographer photographer, Client client){
        this.comment = comment;
        this.date = date;
        this.rating = rating;
        this.photographer = photographer;
        this.client = client;
    }
}

