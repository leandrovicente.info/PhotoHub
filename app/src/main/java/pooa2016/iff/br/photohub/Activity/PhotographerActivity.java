package pooa2016.iff.br.photohub.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pooa2016.iff.br.photohub.Adapter.PhotographerReviewsAdapter;
import pooa2016.iff.br.photohub.Model.Photographer;
import pooa2016.iff.br.photohub.Model.PhotographerReview;
import pooa2016.iff.br.photohub.R;

public class PhotographerActivity extends AppCompatActivity {

    TextView txtDisplayName, txtBio, txtLocation, txtContact, txtPortfolio;
    Button btnQuote, btnAddReview;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photographer);

        //Get ID of selected photographer and retrieves its records from database
        Intent intent = getIntent();
        String raw_id = (String) intent.getSerializableExtra("id");
        id = Integer.parseInt(raw_id);
        Photographer photographer = Photographer.findById(Photographer.class,id);

        //Fills in list of photographer reviews
        List<PhotographerReview> reviews = photographer.getPhotographerReviews();
        ListView list = (ListView) findViewById(R.id.lvPhotographerReviews);
        ArrayAdapter <PhotographerReview> adapter = new PhotographerReviewsAdapter(this, (ArrayList<PhotographerReview>) reviews);
        try{
            list.setAdapter(adapter);
        }catch (Exception e){
            Toast.makeText(this, "Failed to load reviews"+e.getMessage(), Toast.LENGTH_LONG).show();
        }


        //Displays photographer info on page
        String displayName = photographer.getDisplayName();
        String bio = photographer.getBio();
        String city = photographer.getCity();
        String state = photographer.getState();
        String country = photographer.getCountry();
        String phone = photographer.getPhone();
        String email = photographer.getEmail();
        String portfolio = photographer.getPortfolio();

        String location = city + " - " + state + ", " + country;
        String contact = phone + " | " + email;

        txtDisplayName = (TextView) findViewById(R.id.txtQuoteName);
        txtDisplayName.setText(displayName);

        txtBio = (TextView) findViewById(R.id.txtAuthor);
        txtBio.setText(bio);

        txtLocation = (TextView) findViewById(R.id.txtLocation);
        txtLocation.setText(location);

        txtContact = (TextView) findViewById(R.id.txtContact);
        txtContact.setText(contact);

        txtPortfolio = (TextView) findViewById(R.id.txtPortfolio);
        txtPortfolio.setText(portfolio);

        //Define action to add new review
        btnAddReview = (Button) findViewById(R.id.btnAddReview);
        btnAddReview.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){

                Intent intent = new Intent(PhotographerActivity.this, NewPhotographerReviewActivity.class);
                intent.putExtra("id",id);
                startActivity(intent);
                finish();
            }
        });

        //Define action to add new quote
        btnQuote = (Button) findViewById(R.id.btnQuote);
        btnQuote.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(PhotographerActivity.this, NewPriceQuoteActivity.class);
                intent.putExtra("id",id);
                startActivity(intent);
                finish();
            }
        });

    }

}
