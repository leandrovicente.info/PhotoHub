package pooa2016.iff.br.photohub.Model;

import com.orm.SugarRecord;


/**
 * Created by levi on 29/09/16.
 */
public class PhotographerReview extends SugarRecord {
    private String comment,date;

    private Client client;

    private Photographer photographer;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Photographer getPhotographer() {
        return photographer;
    }

    public void setPhotographer(Photographer photographer) {
        this.photographer = photographer;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private int rating;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public PhotographerReview(){}

    public PhotographerReview(String comment, String date, int rating, Client client, Photographer photographer){
        this.comment = comment;
        this.date = date;
        this.rating = rating;
        this.client = client;
        this.photographer = photographer;
    }
}
