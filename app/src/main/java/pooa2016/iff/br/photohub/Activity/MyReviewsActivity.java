package pooa2016.iff.br.photohub.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pooa2016.iff.br.photohub.Adapter.ClientReviewsAdapter;
import pooa2016.iff.br.photohub.Adapter.PhotographerReviewsAdapter;
import pooa2016.iff.br.photohub.Extra.Session;
import pooa2016.iff.br.photohub.Model.Client;
import pooa2016.iff.br.photohub.Model.ClientReview;
import pooa2016.iff.br.photohub.Model.Photographer;
import pooa2016.iff.br.photohub.Model.PhotographerReview;
import pooa2016.iff.br.photohub.R;

public class MyReviewsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_reviews);
        ListView list = (ListView) findViewById(R.id.lvMyReviews);

        if (Session.getType(this).equals("photographer")){

            Photographer photographer = Photographer.findById(Photographer.class,Session.getUser(this));
            List<PhotographerReview> reviews = photographer.getPhotographerReviews();
            ArrayAdapter<PhotographerReview> adapter = new PhotographerReviewsAdapter(this, (ArrayList<PhotographerReview>) reviews);
            try{
                list.setAdapter(adapter);
            }catch (Exception e){
                Toast.makeText(this, "Failed to load reviews"+e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }else if (Session.getType(this).equals("client")){
            Client client = Client.findById(Client.class,Session.getUser(this));
            List<ClientReview> reviews = client.getClientReviews();
            ArrayAdapter<ClientReview> adapter = new ClientReviewsAdapter(this, (ArrayList<ClientReview>) reviews);
            try{
                list.setAdapter(adapter);
            }catch (Exception e){
                Toast.makeText(this, "Failed to load reviews"+e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(this, "Failed to load reviews", Toast.LENGTH_LONG).show();
        }

        //Define action to add new review
        Button btnCancel= (Button) findViewById(R.id.btnReturn);
        btnCancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                finish();
            }
        });
    }
}
