package pooa2016.iff.br.photohub.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import pooa2016.iff.br.photohub.Model.PriceQuote;
import pooa2016.iff.br.photohub.R;

public class ReviewQuoteActivity extends AppCompatActivity {

    TextView txtAuthor, txtContact, txtLocation, txtDescription, txtQuoteName, txtId;
    EditText eTotal;
    Button btnUpdate, btnRefuse, btnViewClient;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_quote);

        //Get ID of selected quote to review
        Intent intent = getIntent();
        String raw_id = (String) intent.getSerializableExtra("id");
        id = Integer.parseInt(raw_id);
        final PriceQuote quote = PriceQuote.findById(PriceQuote.class,id);

        //Info to display on the next activity

        String author = quote.getClient().getName();
        String city = quote.getClient().getCity();
        String state = quote.getClient().getState();
        String country = quote.getClient().getCountry();
        String phone = quote.getClient().getPhone();
        String email = quote.getClient().getEmail();
        String location = city + " - " + state + ", " + country;
        String contact = phone + " | " + email;
        String description = quote.getDescription();
        String status = quote.getStatus();
        String total = quote.getTotal() != null ? quote.getTotal().toString() : "";
        final String client_id = quote.getClient().getId().toString();


        txtQuoteName = (TextView) findViewById(R.id.txtQuoteName);
        txtQuoteName.setText("Quote | Status: "+status);

        txtAuthor = (TextView) findViewById(R.id.txtAuthor);
        txtAuthor.setText("Author: "+author);

        txtLocation = (TextView) findViewById(R.id.txtLocation);
        txtLocation.setText(location);

        txtContact = (TextView) findViewById(R.id.txtContact);
        txtContact.setText(contact);

        txtDescription = (TextView) findViewById(R.id.txtDescription);
        txtDescription.setText("Description: \n"+description);

        eTotal = (EditText) findViewById(R.id.eTotal);
        eTotal.setText(total);

        txtId = (TextView) findViewById(R.id.txtId);
        txtId.setText(client_id);

        //Define action to update price quote
        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){

                String check_total = eTotal.getText().toString();
                if (check_total == null || check_total.equals("0") || check_total.equals("")){
                    Toast.makeText(ReviewQuoteActivity.this, "Please, type a value!", Toast.LENGTH_LONG).show();
                    return;
                }

                Float total = Float.parseFloat(check_total);

                String status = "accepted";

                try{
                    quote.setStatus(status);
                    quote.setTotal(total);
                    quote.save();

                    Intent intent = new Intent(ReviewQuoteActivity.this,HomePhotographerActivity.class);
                    startActivity(intent);
                    Toast.makeText(ReviewQuoteActivity.this, "Quote updated!", Toast.LENGTH_LONG).show();
                    finish();
                }catch (Exception e){
                    Toast.makeText(ReviewQuoteActivity.this, "Failed to update quote!", Toast.LENGTH_LONG).show();
                }
            }
        });

        //Define action to refuse button
        btnRefuse = (Button) findViewById(R.id.btnRefuse);
        btnRefuse.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){

                String status = "refused";

                try{
                    quote.setStatus(status);
                    quote.setTotal(null);
                    quote.save();

                    Intent intent = new Intent(ReviewQuoteActivity.this,HomePhotographerActivity.class);
                    intent.putExtra("id",String.valueOf(id));
                    startActivity(intent);
                    Toast.makeText(ReviewQuoteActivity.this, "Quote Refused!", Toast.LENGTH_LONG).show();
                    finish();
                }catch (Exception e){
                    Toast.makeText(ReviewQuoteActivity.this, "Failed to update quote!", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnViewClient = (Button) findViewById(R.id.btnViewClient);

        btnViewClient.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(ReviewQuoteActivity.this,ClientActivity.class);
                intent.putExtra("id",String.valueOf(client_id));
                startActivity(intent);
            }
        });
    }
}
