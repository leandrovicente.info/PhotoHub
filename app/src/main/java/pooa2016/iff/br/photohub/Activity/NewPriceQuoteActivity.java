package pooa2016.iff.br.photohub.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import pooa2016.iff.br.photohub.Extra.Session;
import pooa2016.iff.br.photohub.Model.Client;
import pooa2016.iff.br.photohub.Model.Photographer;
import pooa2016.iff.br.photohub.Model.PriceQuote;
import pooa2016.iff.br.photohub.R;

public class NewPriceQuoteActivity extends AppCompatActivity {
    TextView txtDisplayName, txtBio, txtLocation, txtContact, txtPortfolio;
    EditText eDescription;
    Button btnSubmitQuote, btnCancel;

    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_price_quote);

        final Client client = Client.findById(Client.class,Session.getUser(this));

        Intent intent = getIntent();
        id = (int) intent.getSerializableExtra("id");

        final Photographer photographer = Photographer.findById(Photographer.class,id);

        String displayName = photographer.getDisplayName();
        String bio = photographer.getBio();
        String city = photographer.getCity();
        String state = photographer.getState();
        String country = photographer.getCountry();
        String phone = photographer.getPhone();
        String email = photographer.getEmail();
        String portfolio = photographer.getPortfolio();

        String location = city + " - " + state + ", " + country;
        String contact = phone + " | " + email;

        txtDisplayName = (TextView) findViewById(R.id.txtName);
        txtDisplayName.setText(displayName);

        txtBio = (TextView) findViewById(R.id.txtBio);
        txtBio.setText(bio);

        txtLocation = (TextView) findViewById(R.id.txtLocation);
        txtLocation.setText(location);

        txtContact = (TextView) findViewById(R.id.txtContact);
        txtContact.setText(contact);

        txtPortfolio = (TextView) findViewById(R.id.txtPortfolio);
        txtPortfolio.setText(portfolio);

        btnSubmitQuote = (Button) findViewById(R.id.btnSubmitQuote);

        btnSubmitQuote.setOnClickListener(new View.OnClickListener(){

            public void onClick(View view){
                eDescription = (EditText) findViewById(R.id.eDescription);
                String description = eDescription.getText().toString();

                try{
                    PriceQuote quote = new PriceQuote(description, "open", null, client,photographer);
                    quote.save();
                    Toast.makeText(NewPriceQuoteActivity.this, "Quote sent!", Toast.LENGTH_LONG).show();
                    finish();
                }catch (Exception e){
                    Toast.makeText(NewPriceQuoteActivity.this, "Failed to add quote!", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnCancel = (Button) findViewById(R.id.btnCancel);

        btnCancel.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                finish();
            }
        });
    }
}
