package pooa2016.iff.br.photohub.Model;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by levi on 19/09/16.
 */
public class Client extends SugarRecord {

    private String name;
    private String phone;
    private String city;
    private String state;
    private String country;
    private String email;
    private String password;



    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PhotographerReview> getPhotographerReviews(){
        return PhotographerReview.find(PhotographerReview.class, "client = ?", String.valueOf(getId()));
    }

    public List<ClientReview> getClientReviews(){
        return ClientReview.find(ClientReview.class, "client = ?", String.valueOf(getId()));
    }

    public List<PriceQuote> getPriceQuotes(){
        return PriceQuote.find(PriceQuote.class,"client = ?", String.valueOf(getId()));
    }

    public Client(){

    }

    public Client(String name, String phone, String city, String state, String country, String email, String password){
        this.name = name;
        this.phone = phone;
        this.city = city;
        this.state = state;
        this.country = country;
        this.email = email;
        this.password = password;
    }

}
