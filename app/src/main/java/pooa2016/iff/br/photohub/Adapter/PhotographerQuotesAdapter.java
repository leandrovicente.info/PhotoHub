package pooa2016.iff.br.photohub.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import pooa2016.iff.br.photohub.Model.PriceQuote;
import pooa2016.iff.br.photohub.R;

/**
 * Created by levi on 30/09/16.
 */
public class PhotographerQuotesAdapter extends ArrayAdapter<PriceQuote>{

    private Context context = null;
    private ArrayList<PriceQuote> quotes = null;

    public PhotographerQuotesAdapter(Context context, ArrayList<PriceQuote> quotes){
        super(context, R.layout.quotes_list, quotes);
        this.context = context;
        this.quotes = quotes;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.quotes_list, parent, false);

        TextView txtDescription = (TextView) rowView.findViewById(R.id.txtPrice);
        TextView txtAuthor = (TextView) rowView.findViewById(R.id.txtAuthor);
        TextView txtId = (TextView) rowView.findViewById(R.id.txtId);
        TextView txtTotal = (TextView) rowView.findViewById(R.id.txtTotal);
        TextView txtStatus = (TextView) rowView.findViewById(R.id.txtStatus);

        String total = quotes.get(position).getTotal() != null ? quotes.get(position).getTotal().toString() : "";

        txtDescription.setText("Description: "+quotes.get(position).getDescription());
        txtAuthor.setText("Author: "+quotes.get(position).getClient().getName());
        txtId.setText(quotes.get(position).getId().toString());
        txtStatus.setText("Status: "+quotes.get(position).getStatus());
        txtTotal.setText("Total: "+total);

        return rowView;
    }
}
