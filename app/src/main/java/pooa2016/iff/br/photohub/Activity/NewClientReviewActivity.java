package pooa2016.iff.br.photohub.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

import pooa2016.iff.br.photohub.Extra.Session;
import pooa2016.iff.br.photohub.Model.Client;
import pooa2016.iff.br.photohub.Model.Photographer;
import pooa2016.iff.br.photohub.Model.ClientReview;
import pooa2016.iff.br.photohub.R;


public class NewClientReviewActivity extends AppCompatActivity {

    TextView txtName, txtLocation, txtContact;
    RatingBar rRating;
    EditText eComment;
    Button btnSubmitReview, btnCancelReview;

    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_client_review);

        final Photographer photographer = Photographer.findById(Photographer.class,Session.getUser(this));

        Intent intent = getIntent();
        id = (int) intent.getSerializableExtra("id");

        final Client client = Client.findById(Client.class,id);

        String name = client.getName();
        String city = client.getCity();
        String state = client.getState();
        String country = client.getCountry();
        String phone = client.getPhone();
        String email = client.getEmail();

        String location = city + " - " + state + ", " + country;
        String contact = phone + " | " + email;

        txtName = (TextView) findViewById(R.id.txtName);
        txtName.setText(name);

        txtLocation = (TextView) findViewById(R.id.txtLocation);
        txtLocation.setText(location);

        txtContact = (TextView) findViewById(R.id.txtContact);
        txtContact.setText(contact);

        btnSubmitReview = (Button) findViewById(R.id.btnSubmitReview);

        btnSubmitReview.setOnClickListener(new View.OnClickListener(){

            public void onClick(View view){
                rRating = (RatingBar) findViewById(R.id.rRating);
                eComment = (EditText) findViewById(R.id.eComment);

                String comment = eComment.getText().toString();
                String date;

                int rating = Math.round(rRating.getRating());

                SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
                date = s.format(new Date());

                try{
                    ClientReview review = new ClientReview(comment, date, rating, photographer,client);
                    review.save();
                    Toast.makeText(NewClientReviewActivity.this, "Review added!", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(NewClientReviewActivity.this,ClientActivity.class);
                    intent.putExtra("id",String.valueOf(id));
                    startActivity(intent);
                    finish();
                }catch (Exception e){
                    Toast.makeText(NewClientReviewActivity.this, "Failed to add review!", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnCancelReview = (Button) findViewById(R.id.btnCancelReview);

        btnCancelReview.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {

                Intent intent = new Intent(NewClientReviewActivity.this,ClientActivity.class);
                intent.putExtra("id",String.valueOf(id));
                startActivity(intent);
                finish();
            }
        });
    }
}
