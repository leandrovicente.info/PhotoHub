package pooa2016.iff.br.photohub.Model;

import com.orm.SugarRecord;

/**
 * Created by levi on 30/09/16.
 */
public class PriceQuote extends SugarRecord {
    private String description, status;
    private Float total;
    private Client client;
    private Photographer photographer;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Photographer getPhotographer() {
        return photographer;
    }

    public void setPhotographer(Photographer photographer) {
        this.photographer = photographer;
    }

    public PriceQuote(){}

    public PriceQuote(String description, String status, Float total, Client client, Photographer photographer){
        this.description = description;
        this.status = status;
        this.total = total;
        this.client = client;
        this.photographer = photographer;
    }
}
