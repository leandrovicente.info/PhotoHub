package pooa2016.iff.br.photohub.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import pooa2016.iff.br.photohub.Model.ClientReview;
import pooa2016.iff.br.photohub.R;

/**
 * Created by levi on 30/09/16.
 */
public class ClientReviewsAdapter extends ArrayAdapter<ClientReview> {

    private Context context = null;
    private ArrayList<ClientReview> reviews = null;

    public ClientReviewsAdapter(Context context, ArrayList<ClientReview> reviews){
        super(context, R.layout.client_reviews_list, reviews);
        this.context = context;
        this.reviews = reviews;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.client_reviews_list, parent, false);

        TextView rating = (TextView) rowView.findViewById(R.id.txtRating);
        TextView comment = (TextView) rowView.findViewById(R.id.txtComment);
        TextView author = (TextView) rowView.findViewById(R.id.txtAuthor);
        TextView date = (TextView) rowView.findViewById(R.id.txtDate);

        rating.setText("Rating: "+ Integer.toString(reviews.get(position).getRating()));
        comment.setText("Comment: "+ reviews.get(position).getComment());
        author.setText("Author: "+reviews.get(position).getPhotographer().getName());
        date.setText("Date: "+reviews.get(position).getDate());

        return rowView;
    }
}
