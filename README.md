# PHOTOHUB - Click and Connect 

Android app developed as part of the Applied Object Oriented Programming class at Instituto Federal Fluminense (Campos dos Goytacazes, RJ - Brazil).

This is an academic project. 

How it works?

The user signs in as a photographer or as a client. 

A client browses the photographers catalog and on a photographer page they can request a price quote or leave a review.

A photographers has access to all reviews and price quote requests sent to him. They cand deny the quote or update it with a total price. 

Photographers review clients as well and the clients have access to all left on to their accounts.

